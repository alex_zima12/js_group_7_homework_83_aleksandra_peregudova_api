const express = require('express');
const router = express.Router();
const TrackHistory = require("../models/TrackHistory");
const User = require("../models/User");

const createRouter = () => {

    router.post("/", async (req, res) => {
        const token = req.get("Authorization")
        if (!token) {
            return res.status(401).send({error: "No token presented"});
        }
        const userByToken = await User.findOne({token});
        const user = userByToken._id;
        const userRes = {...req.body,user}
        const trackHistoryData = new TrackHistory(userRes);
        if (!user) {
            return res.status(401).send({error: "Wrong token"});
        }
        try {
            await trackHistoryData.save();
            res.send(trackHistoryData);
        } catch (e) {
            res.sendStatus(400);
        }
    });

    return router;
};

module.exports = createRouter;