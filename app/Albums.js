const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const Album = require('../models/Album');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = () => {
    router.get("/", async (req, res) => {
        let album;
        try {
            if (req.query.artist) {
                album = await Album.find({"artist": req.query.artist})
            } else {
                album = await Album.find()
            }
            res.send(album);
        } catch (e) {
            res.sendStatus(500);
        }
    });
    router.get("/:id", async (req, res) => {
        try {
            let album = await Album.findById(req.params.id).populate('artist');
            res.send(album);
        } catch (e) {
            res.status(404).send('404 not found');
        }

             // let query = await Albums.findById(req.params.id);
        // // let query;
        // if (req.query.artist) {
        //     query = {artist: req.query.artist}
        // }
        // if (query) {
        //     const albums = await Albums.find(query).populate('artist');
        //     res.send(albums);
        // }
    });
    router.post("/", upload.single("image"), async (req, res) => {
        const albumData = new Album(req.body);
        if (req.file) {
            albumData.image = req.file.filename;
        }
        try {
            await albumData.save();
            res.send(albumData);
        } catch (e) {
            res.status(400).send(e);
        }
    });
    return router;
};

module.exports = createRouter;