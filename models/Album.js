const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AlbumSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    artist: {
        type: Schema.Types.ObjectId,
        ref: "Artist",
        require: true
    },
    image: String,
    date: {
        type: String,
        required: true
    },
});

const Album = mongoose.model("Album", AlbumSchema);

module.exports = Album;