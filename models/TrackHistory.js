const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TrackHistorySchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: "User",
        require: true
    },
    track: {
        type: Schema.Types.ObjectId,
        ref: "Track",
        require: true
    },
    datetime: {
        type : Date,
        default: Date.now
    }
});

const Track = mongoose.model("TrackHistory", TrackHistorySchema);

module.exports = Track;